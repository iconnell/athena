#!/bin/sh
#
# art-description: Athena runs topoclustering from an ESD file
# art-type: grid
# art-include: 21.0/Athena
# art-include: 21.0-TrigMC/Athena
# art-include: master/Athena
# art-include: 21.3/Athena
# art-include: 21.9/Athena

athena RecExRecoTest/RecExRecoTest_ART_caloTopoClustering_fromESD.py
echo "art-result: $?"
